from django.db import models

class Data(models.Model):
    objectid = models.IntegerField()
    gen = models.CharField(max_length=60)
    cases = models.FloatField(max_length=30)
    phase = models.IntegerField()
    time = models.DateField()

    def __str__(self):
        return '{} {} {} {} {},'.format(self.objectid, self.gen, round(self.cases,1), self.phase, self.time)

    def get_name(self):
        return '{}'.format(self.gen)

    def get_objectid(self):
        return '{:}'.format(self.objectid)

class Data_BL(models.Model):
    objectid = models.IntegerField()
    gen = models.CharField(max_length=60)
    cases = models.FloatField(max_length=30)
    time = models.DateField()

    def __str__(self):
        return '{} {} {} {} {},'.format(self.objectid, self.gen, round(self.cases,1), self.time)
from django.shortcuts import render, get_object_or_404, redirect
from django.conf import settings
from django.http import HttpResponse, Http404, HttpResponseRedirect
from .models import Data, Data_BL
from .forms import StatistikForm
from datetime import date, timedelta, datetime
from dateutil import parser
from .templates import add_ov, wrapper, query_DB, get_neighbor, query_DB_Chart, generate_dataset, query_DB_BL, query_DB_Chart_statistic
from .templates import query_DB_BL_YT, get_change, query_DB_Chart_BL, get_all_kreise, lv_liste


lv_longNames_dict = {
    'nw' : 'NW',
    'bw' : 'BW',
    'by' : 'BY',
    'be' : 'BEBBST',
    'ni' : 'HBNI',
    'sh' : 'HHMVSH',
    'rp' : 'HERPSL',
    'th' : 'SNTH',
    'az' : 'AZ',
}
lv_BL_dict = {
    'bw' : [1],
    'by' : [2],
    'be' : [3, 4, 14],
    'ni' : [5, 9],
    'sh' : [6, 8, 15],
    'rp' : [7, 11, 12],
    'th' : [13, 16],
    'nw' : [10],
    
}
def landing_page(request,lv_name="nw",rb_name="Köln"):
    try:
        if rb_name.lower() == 'frankfurt(oder)':
            converted_rb_name = 'Frankfurt (Oder)'
        elif rb_name.lower() == 'badkreuznach':
            converted_rb_name = 'Bad Kreuznach'
        elif rb_name.lower() == 'neustadt':
            converted_rb_name = 'Neustadt a.d.W.'
        elif rb_name.lower() == 'badtölz':
            converted_rb_name = 'Bad Tölz'
        elif rb_name.lower() == 'villingen-schwenningen':
            converted_rb_name = 'Villingen-Schwenningen'
        else:
            converted_rb_name = rb_name[0].upper() + rb_name[1:].lower()
        lv_liste[lv_name.lower()].index(converted_rb_name)
        corona_data = query_DB_Chart(converted_rb_name)
        corona_data_today,corona_data_yesterday  = query_DB() ##this could be a bottle neck
        rb_result = add_ov(converted_rb_name, corona_data_today, corona_data_yesterday)
        neigbor_result = get_neighbor(converted_rb_name, corona_data_today, corona_data_yesterday)
        label,dataset = generate_dataset(corona_data)
    except:
        return render(request, '404.html',status=404)
    return render(request, 'index.html', {
                                        'rb_name': "RB "+ converted_rb_name,
                                        'date': datetime.now().strftime("%d.%m.%Y %H:%M"),
                                        'rb_data': rb_result,
                                        'neighbor_data': neigbor_result,
                                        'label': label,
                                        'dataset' : dataset
                                        })

def lv_list(request, lv_name):
    try:
        converted_lv_name = lv_name.lower()
        prefix = '' if converted_lv_name == 'az' else 'LV'
        corona_data_today,corona_data_yesterday = query_DB()
        cases_in_lv = ""
        if converted_lv_name != 'az':
            corona_data_today_BL = query_DB_BL(lv_BL_dict[converted_lv_name])
            cases_in_lv_temp = []
            for item in corona_data_today_BL:
                cases_in_lv_temp.append(item['gen'] + ": " + str(round(item['cases'],1)))
            cases_in_lv = "(" +', '.join(cases_in_lv_temp) + ")"
        lv_result = sorted(wrapper(lv_liste[converted_lv_name], corona_data_today, corona_data_yesterday), key=lambda k: k['gen'])
    except:
        return render(request, '404.html',status=404)
    return render(request, 'lv_list.html', {
                                        'lv_name':prefix + ' ' + lv_longNames_dict[converted_lv_name],
                                        'date': datetime.now().strftime("%d.%m.%Y %H:%M"),
                                        'lv_data': lv_result,
                                        # 'rb_in_lv' : lv_liste[lv_name],
                                        'cases_in_lv' : cases_in_lv,
                                        })

def de_list(request):
    bl_list = []
    cases_in_de = 0
    for item in lv_BL_dict:
        bl_list += (lv_BL_dict[item])
    bl_list = sorted(bl_list)
    corona_data_today,corona_data_yesterday = query_DB_BL_YT(bl_list)
    for item in corona_data_today:
        cases_in_de += item['cases']
        item['change'] = get_change(item,corona_data_yesterday[item['objectid']-1])
    corona_data = query_DB_Chart_BL()
    label,dataset = generate_dataset(corona_data,21)
    return render(request, 'bl_list.html', {
                                        'date': datetime.now().strftime("%d.%m.%Y %H:%M"),
                                        'lv_data': corona_data_today,
                                        'cases_in_de' : round(cases_in_de/len(bl_list),1),
                                        'label': label,
                                        'dataset' : dataset
                                        })


def hinweis(request):
    return render(request, 'hinweis.html')

def uebersicht(request):
    host = request.build_absolute_uri('/')
    all_gens = get_all_kreise()
    return render(request, 'uebersicht.html',{
                            'lv_data': all_gens,
                            'host' : host
    })

def statistik(request):
    if request.method == 'POST':
        form = StatistikForm(request.POST)
        if form.is_valid():
            try:
                begin_date = parser.parse(request.POST.get('begin_date'))
            except:
                begin_date = datetime.now()
            try:
                end_date = parser.parse(request.POST.get('end_date'))
            except:
                end_date = datetime.now()
            date_count = end_date - begin_date
            label,data = generate_dataset(query_DB_Chart_statistic(form.cleaned_data['rb_select'],begin_date,end_date),abs(date_count.days))
            form = StatistikForm(request.POST)

            return render(request, 'statistiken.html', {
                                                        'form' : form,
                                                        'label' : label,
                                                        'dataset': data,
                                                        'method' : True,
                                                        'begin' : label[0],
                                                        'end' : end_date.strftime("%d.%m.%Y")
                                                        })

    # if a GET (or any other method) we'll create a blank form
    else:
        form = StatistikForm()

    return render(request, 'statistiken.html', {
                                                'form': form,
                                                'method' : False
                                                })
    
function sortTable(rowNr,type, tableName, rb_length) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, counter=0;
    table = document.getElementById(tableName);
    switching = true;
    /*Make a loop that will continue until
    no switching has been done:*/
    dir = "asc"; 
    while (switching) {
      //start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      /*Loop through all table rows (except the
      first, which contains table headers):*/
      for (i = 2; i < (rb_length+1); i++) {
        //start by saying there should be no switching:
        shouldSwitch = false;
        /*Get the two elements you want to compare,
        one from current row and one from the next:*/
        x = rows[i].getElementsByTagName("TD")[rowNr];
        y = rows[i + 1].getElementsByTagName("TD")[rowNr];
        if (dir == "asc") {
          if (type == 0){
            //check if the two rows should switch place:
            if (Number(x.innerHTML) > Number(y.innerHTML)) {
              //if so, mark as a switch and break the loop:
              shouldSwitch = true;
              break;
            }
          } else if (type == 1){
              if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
              // If so, mark as a switch and break the loop:
              shouldSwitch = true;
              break;
            }
          }
        } else if (dir == "desc") {
          if (type == 0){
            if (Number(x.innerHTML) < Number(y.innerHTML)) {
              // If so, mark as a switch and break the loop:
              shouldSwitch = true;
              break;
            }
          } else if (type == 1){
            if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
              // If so, mark as a switch and break the loop:
              shouldSwitch = true;
              break;
            }
          }
        }
      }
      if (shouldSwitch) {
        /*If a switch has been marked, make the switch
        and mark that a switch has been done:*/
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        counter++;
      } else {
        /* If no switching has been done AND the direction is "asc",
        set the direction to "desc" and run the while loop again. */
        if (counter == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  };

  function Search(tableName, offset) {
    // Declare variables
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById(tableName);
    tr = table.getElementsByTagName("tr");
  
    // Loop through all table rows, and hide those who don't match the search query
    for (i = offset; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[0];
      td1 = tr[i].getElementsByTagName("td")[1];
      is_present = 0;

      if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
              is_present = 1
          }
      }
       if (td1) {
          if (td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
              is_present = 1
          }
      }

      if (is_present == 1) {
          tr[i].style.display = "";

          } else {
              tr[i].style.display = "none";
      }
    }
  };
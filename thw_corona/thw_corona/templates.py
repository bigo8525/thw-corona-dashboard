from datetime import date, timedelta, datetime
from .models import Data, Data_BL

lv_liste = {
    'nw' : ['Aachen','Arnsberg','Bielefeld','Bochum','Dortmund','Düsseldorf','Gelsenkirchen','Köln','Mönchengladbach','Münster','Olpe','Wesel'],
    'bw' : ['Biberach','Freiburg','Göppingen','Heilbronn','Karlsruhe','Mannheim','Stuttgart','Tübingen','Villingen-Schwenningen'],
    'by' : ['Bad Tölz','Bamberg','Hof','Ingolstadt','Karlstadt','Kempten','Mühldorf','München','Nürnberg','Schwandorf','Straubing'],
    'be' : ['Berlin','Frankfurt (Oder)','Halle','Magdeburg','Potsdam'],
    'ni' : ['Braunschweig','Bremen','Buxtehude','Göttingen','Hannover','Lingen','Oldenburg','Verden'],
    'sh' : ['Hamburg','Lübeck','Neumünster','Schleswig','Schwerin','Stralsund'],
    'rp' : ['Bad Kreuznach','Darmstadt','Frankfurt','Gelnhausen','Gießen','Homberg','Koblenz','Merzig','Neustadt a.d.W.','Saarbrücken','Trier'],
    'th' : ['Chemnitz','Dresden','Erfurt','Leipzig'],
    'az' : ['Hoya','Neuhausen','Brandenburg'],
    }
color_list = ["#e6194b", "#3cb44b", "#ffe119", "#4363d8", "#f58231", "#911eb4", "#46f0f0", "#f032e6", "#bcf60c", "#fabebe", "#008080", "#e6beff", "#9a6324", "#c9c065", "#800000", "#aaffc3", "#808000", "#ffd8b1", "#000075", "#808080", "#ffffff"]
rb_nachbar_ids = {
    'Aachen' : [76, 84, 86, 89, 144, 157],
    'Arnsberg' : [23, 31, 97, 99, 102, 108, 112, 113, 114, 116, 139, 141],
    'Bielefeld' : [28, 29, 31, 32, 33, 48, 58, 97, 101, 104, 115],
    'Bochum' : [66, 72, 75, 87, 88, 91, 95, 106, 112, 116],
    'Dortmund' : [87, 94, 95, 97, 105, 107, 110, 111, 113, 115],
    'Düsseldorf' : [66, 67, 70, 71, 73, 76, 78, 80, 81, 88, 110],
    'Gelsenkirchen' : [69, 70, 75, 78, 93, 94, 105, 106, 109, 110, 116],
    'Köln' : [72, 75, 85, 87, 144, 145, 150],
    'Mönchengladbach' : [64, 65, 74, 75, 78, 80, 82, 83, 84],
    'Münster' : [53, 55, 58, 93, 95, 99, 108, 115, 116],
    'Olpe' : [71, 72, 73, 88, 89, 110, 111, 112, 132, 134, 141, 145, 153],
    'Wesel' : [55, 65, 67, 77, 90, 94, 95, 96],
    'Berlin' : [330, 331, 333, 334, 335, 337, 339, 412, 415],
    'Frankfurt (Oder)' : [332, 335, 342, 346, 350, 358, 359, 360, 406, 407, 408, 409, 410],
    'Halle' : [332, 339, 342, 363, 364, 372, 373, 376, 386, 389, 392, 395, 398, 400, 401],
    'Magdeburg' : [20, 21, 22, 25, 37, 43, 333, 338, 339, 340, 369, 374, 375, 386],
    'Potsdam' : [37, 330, 331, 336, 343, 360, 364, 369, 373, 377, 378, 405, 408, 411, 413, 414],
    'Chemnitz' : [276, 360, 361, 363, 364, 399, 400, 401],
    'Dresden' : [332, 336, 341, 354, 364],
    'Erfurt' : [21, 26, 137, 138, 142, 274, 277, 298, 299, 371, 372, 374, 375, 380, 399, 400, 401],
    'Leipzig' : [276, 277, 332, 354, 355, 356, 360, 369, 371, 375, 378, 397, 398],
    'Hamburg' : [6, 9, 13, 15, 36, 42],
    'Lübeck' : [2, 4, 11, 13, 16, 36, 38, 349, 351],
    'Neumünster' : [7, 8, 10, 11, 12, 15, 16],
    'Schleswig' : [5, 10, 14],
    'Schwerin' : [3, 6, 37, 38, 340, 346, 348],
    'Stralsund' : [335, 338, 343, 351, 374],
    'Braunschweig' : [21, 27, 29, 31, 43, 368, 370, 372],
    'Bremen' : [28, 40, 42, 44, 47, 52, 59, 60],
    'Buxtehude' : [6, 16, 20, 34, 35, 39, 41, 44, 340, 351, 368, 377],
    'Göttingen' : [25, 29, 30, 101, 102, 136, 139, 142, 372, 385, 386],
    'Hannover' : [20, 24, 30, 31, 32, 34, 41, 102, 103],
    'Lingen' : [28, 50, 56, 57, 93, 96, 97, 99, 100, 103],
    'Oldenburg' : [35, 39, 45, 52, 53, 57, 62],
    'Verden' : [16, 20, 27, 33, 38, 39, 40, 43, 45, 57, 58, 59, 62, 103],
    'Koblenz' : [87, 89, 113, 114, 129, 132, 133, 148, 151, 157, 177],
    'Trier' : [85, 144, 147, 149, 151, 320, 324],
    'Saarbrücken' : [168, 174, 178, 322, 324],
    'Merzig' : [147, 158, 174, 319, 321],
    'Bad Kreuznach' : [121, 123, 126, 129, 148, 149, 152, 155, 158, 164, 173, 176, 323, 324],
    'Darmstadt' : [118, 120, 126, 128, 163, 167, 167, 169, 177, 197, 198, 199, 296, 301],
    'Frankfurt' : [117, 122, 123, 125, 131, 132, 133, 135, 152, 163, 177, 296],
    'Gelnhausen' : [118, 119, 128, 130, 131, 134, 138, 140, 296, 297, 298, 302, 387, 390],
    'Gießen' : [101, 111, 114, 124, 129, 130, 135, 139, 140, 152, 153],
    'Homberg' : [23, 26, 101, 134, 135, 137, 141, 385, 387, 388],
    'Neustadt a.d.W.' : [121, 159, 162, 167, 170, 171, 174, 193, 194, 199, 323],
    'Karlstadt' : [122, 125, 127, 128, 189, 198, 272, 290, 297, 303],
    'Bamberg' : [125, 137, 273, 277, 278, 287, 289, 290, 300, 302, 304, 390, 393, 396],
    'Hof' : [261, 266, 272, 274, 275, 279, 289, 355, 396, 397, 399],
    'Nürnberg' : [188, 189, 191, 272, 275, 289, 291, 292, 300, 304, 317],
    'Schwandorf' : [231, 251, 254, 256, 264, 265, 273, 275, 284, 291],
    'Ingolstadt' : [190, 191, 229, 233, 251, 263, 284, 285, 286, 288, 289, 309, 310, 312],
    'Straubing' : [231, 233, 241, 252, 255, 262, 263, 266],
    'Mühldorf' : [230, 232, 233, 242, 251, 253, 256, 257],
    'München' : [191, 228, 236, 237, 238, 240, 241, 242, 243, 251, 252, 311, 312, 316, 317],
    'Kempten' : [190, 217, 218, 219, 220, 221, 235, 236, 245, 310, 311],
    'Bad Tölz' : [224, 230, 234, 238, 239, 244, 309, 310, 315],
    'Mannheim' : [121, 127, 159, 162, 166, 176, 186, 187, 189, 194, 301],
    'Karlsruhe' : [172, 176, 180, 183, 186, 199, 201, 203, 207],
    'Freiburg' : [195, 203, 208, 209, 213],
    'Villingen-Schwenningen' : [203, 205, 206, 207, 212, 216, 220, 222],
    'Tübingen' : [180, 181, 182, 195, 201, 207, 208, 210, 218, 219, 222],
    'Stuttgart' : [181, 182, 186, 188, 191, 194, 195, 200, 202, 203, 214, 214],
    'Göppingen' : [179, 180, 184, 188, 214, 218, 286, 311, 312, 317],
    'Biberach' : [181, 182, 190, 210, 211, 214, 216, 308, 312, 313, 314, 316, 318],
    'Heilbronn' : [183, 184, 191, 194, 198, 199, 202, 286, 290, 301, 302, 304],
}

rb_ids = {
    'Hoya' : [32],
    'Neuhausen' : [181],
    'Brandenburg' : [326],
    'Aachen' : [82, 83, 85],
    'Arnsberg' : [101, 104, 111, 115],
    'Bielefeld' : [98, 99, 100, 102, 103],
    'Bochum' : [71, 73, 105, 107, 109, 110],
    'Dortmund' : [106, 108, 112, 116],
    'Düsseldorf' : [64, 65, 69, 72, 75],
    'Gelsenkirchen' : [66, 90, 91, 95],
    'Köln' : [79, 80, 81, 84, 88, 89],
    'Mönchengladbach' : [67, 68, 76, 77, 86],
    'Münster' : [92, 94, 96, 97],
    'Olpe' : [87, 88, 113, 114],
    'Wesel' : [65, 70, 74, 78, 93],
    'Berlin' : [404, 405, 406, 407, 408, 409, 410, 411, 413, 414],
    'Frankfurt (Oder)' : [327, 328, 330, 331, 334, 336, 337, 341, 343],
    'Halle' : [365, 366, 369, 371, 374, 378],
    'Magdeburg' : [367, 368, 370, 372, 373, 375, 376, 377],
    'Potsdam' : [326, 329, 332, 333, 335, 338, 339, 340, 342, 412, 415],
    'Chemnitz' : [352, 353, 354, 355, 356],
    'Dresden' : [357, 358, 359, 360, 361],
    'Erfurt' : [379, 381, 382, 383, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398],
    'Leipzig' : [362, 363, 364, 380, 399, 400, 401],
    'Hamburg' : [16],
    'Lübeck' : [3, 6, 8, 10, 15],
    'Neumünster' : [4, 5, 9, 13, 14],
    'Schleswig' : [1, 2, 7, 11, 12],
    'Schwerin' : [344, 345, 347, 349, 351],
    'Stralsund' : [346, 348, 350],
    'Braunschweig' : [17, 18, 19, 20, 22, 24, 25, 30],
    'Bremen' : [35, 39, 45, 57, 62, 63],
    'Buxtehude' : [36, 37, 38, 40, 42, 43],
    'Göttingen' : [21, 23, 26, 31],
    'Hannover' : [27, 29, 33],
    'Lingen' : [48, 52, 53, 55, 58, 59],
    'Oldenburg' : [46, 47, 49, 50, 51, 54, 56, 60, 61],
    'Verden' : [28, 32, 34, 41, 44],
    'Koblenz' : [143, 144, 145, 148, 149, 150, 151, 152, 153],
    'Trier' : [154, 155, 156, 157, 158],
    'Merzig' : [320, 322, 324],
    'Neustadt a.d.W.' : [160, 161, 164, 165, 166, 168, 170, 172, 173, 175, 176, 178],
    'Saarbrücken' : [319, 321, 323],
    'Bad Kreuznach' : [146, 147, 151, 159, 162, 163, 167, 169, 171, 174, 177],
    'Darmstadt' : [117, 121, 122, 123, 127],
    'Frankfurt' : [118, 119, 120, 124, 126, 128, 129, 130],
    'Gelnhausen' : [125, 135, 137],
    'Gießen' : [131, 132, 133, 134, 141],
    'Homberg' : [136, 138, 139, 140, 142],
    'Karlstadt' : [293, 295, 296, 300, 301, 302, 304],
    'Bamberg' : [268, 270, 272, 274, 275, 279, 294, 297, 298, 299, 303],
    'Hof' : [260, 264, 267, 269, 271, 273, 276, 277, 278, 280],
    'Nürnberg' : [281, 282, 283, 284, 285, 286, 287, 288, 290],
    'Schwandorf' : [258, 261, 262, 263, 266, 289],
    'Ingolstadt' : [223, 231, 240, 241, 291, 292, 311, 317],
    'Straubing' : [247, 248, 249, 250, 251, 253, 254, 256, 257, 259, 265],
    'Mühldorf' : [226, 227, 238, 244, 246, 252, 255],
    'München' : [224, 229, 230, 232, 233, 234, 239, 305, 309, 310],
    'Kempten' : [306, 307, 308, 312, 313, 314, 315, 316, 318],
    'Bad Tölz' : [225, 228, 235, 236, 237, 242, 243, 245],
    'Mannheim' : [196, 197, 198, 199],
    'Karlsruhe' : [192, 193, 194, 195, 200, 202],
    'Freiburg' : [204, 205, 206, 207, 212],
    'Villingen-Schwenningen' : [208, 209, 210, 211, 213],
    'Tübingen' : [203, 214, 215, 216],
    'Stuttgart' : [179, 180, 183, 184, 201],
    'Göppingen' : [181, 182, 190, 191],
    'Biberach' : [217, 218, 219, 220, 221, 222],
    'Heilbronn' : [185, 186, 187, 188, 189],
    }
    
ov_dict = {
#BW
    'Heilbronn' : 'Heilbronn',
    'Heilbronn Landkreis' : 'Weinsberg, Widdern',
    'Hohenlohekreis' : 'Künzelsau, Pfedelbach',
    'Main-Tauber-Kreis' : 'Igersheim, Wertheim',
    'Schwäbisch Hall' : 'Crailsheim, Schwäbisch Hall',
    'Ulm' : 'Ulm',
    'Alb-Donau-Kreis' : 'Blaubeuren, Ehingen',
    'Biberach' : 'Biberach/Riß, Riedlingen',
    'Ravensburg' : 'Wangen, Weingarten',
    'Bodenseekreis' : 'Friedrichshafen, Überlingen',
    'Sigmaringen' : 'Pfullendorf',
    'Esslingen' : 'Kirchheim/Teck, Neuhausen, Ostfildern',
    'Göppingen' : 'Geislingen, Göppingen, Gruibingen',
    'Ostalbkreis' : 'Aalen, Ellwangen, Schwäbisch Gmünd',
    'Heidenheim' : 'Heidenheim',
    'Calw' : 'Calw',
    'Böblingen' : 'Böblingen, Leonberg',
    'Stuttgart' : 'Stuttgart',
    'Ludwigsburg' : 'Bietigheim-Bissingen, Ludwigsburg',
    'Rems-Murr-Kreis' : 'Backnang, Schorndorf',
    'Freudenstadt' : 'Freudenstadt, Horb',
    'Zollernalbkreis' : 'Albstadt, Balingen, Hechingen',
    'Tübingen' : 'Ofterdingen, Rottenburg, Tübingen',
    'Reutlingen' : 'Münsingen, Reutlingen',
    'Waldshut' : 'Bad Säcking, Laufenburg, Waldshut-Tiengen',
    'Schwarzwald-Baar-Kreis' : 'Donaueschingen, Villingen-Schwenningen',
    'Tuttlingen' : 'Trossingen, Tuttlingen',
    'Konstanz' : 'Konstanz, Radolfzell, Singen, Stockach',
    'Rottweil' : 'Rottweil, Schramberg',
    'Ortenaukreis' : 'Achern, Biberach/BD., Kehl, Lahr, Offenburg',
    'Emmendingen' : 'Emmendingen',
    'Breisgau-Hochschwarzwald' : 'Breisach, Müllheim',
    'Freiburg im Breisgau' : 'Freiburg',
    'Lörrach' : 'Lörrach, Rheinfelden, Schopfheim',
    'Karlsruhe' : 'Karlsruhe',
    'Karlsruhe Landkreis' : 'Dettenheim, Oberhausen-Rheinhausen, Waghäusel',
    'Pforzheim' : 'Pforzheim',
    'Baden-Baden' : 'Baden-Baden',
    'Rastatt' : 'Rastatt, Bühl',
    'Enzkreis' : 'Mühlacker, Neuenbürg-Arnbach, Niefern-Öschelbronn',
    'Mannheim' : 'Mannheim',
    'Rhein-Neckar-Kreis' : 'Ladenburg',
    'Wiesloch-Waldorf' : 'Eberbach, Neckargemünd, Sinsheim',
    'Heidelberg' : 'Heidelberg',
    'Neckar-Odenwald-Kreis' : 'Adelsheim, Hassmersheim, Neunkirchen',
#RP
    'Eifelkreis Bitburg-Prüm' : 'Bitburg, Prüm',
    'Cochem-Zell' : 'Cochem, Zell',
    'Vulkaneifel' : 'Daun',
    'Trier-Saarburg' : 'Hermeskeil, Saarburg',
    'Trier' : 'Trier',
    'Bernkastel-Wittlich' : 'Wittlich',
    'Saarpfalz-Kreis' : 'Blieskastel, Homburg, St. Ingbert',
    'Regionalverband Saarbrücken' : 'Heusweiler, Friedrichsthal, Riegelsberg, Saarbrücken, Sulzbach, Völklingen',
    'Neunkirchen' : 'Neunkirchen, Illingen, Spiesen-Elversberg',
    'Südliche Weinstraße' : 'Bad Bergzabern',
    'Germersheim' : 'Germersheim',
    'Südwestpfalz' : 'Hauenstein',
    'Kaiserslautern' : 'Kaiserslautern',
    'Landau in der Pfalz' : 'Landau',
    'Neustadt an der Weinstraße' : 'Neustadt a.d.W.',
    'Pirmasens' : 'Pirmasens',
    'Speyer' : 'Speyer',
    'Zweibrücken' : 'Zweibrücken',
    'Merzig-Wadern' : 'Beckingen, Mettlach, Wadern, Perl',
    'St. Wendel' : 'Freisen, Nohfelden, St. Wendel, Theley',
    'Saarlouis' : 'Dillingen, Lebach, Saarlouis, Saarwellingen',    
    'Ahrweiler' : 'Ahrweiler, Sinzig',
    'Mayen-Koblenz' : 'Andernach, Bendorf, Mayen',
    'Altenkirchen (Westerwald)' : 'Betzdorf',
    'Koblenz' : 'Koblenz',
    'Rhein-Lahn-Kreis' : 'Lahnstein',
    'Westerwaldkreis' : 'Monterbaur, Westerburg',
    'Neuwied' : 'Neuwied',
    'Hersfeld-Rotenburg' : 'Bad Hersfeld, Rotenburg',
    'Werra-Meißner-Kreis' : 'Eschwege, Großalmerode, Witzenhausen',
    'Schwalm-Eder-Kreis' : 'Fritzlar, Homberg, Melsungen, Schwalmstadt',
    'Kassel Landkreis' : 'Hofgeismar, Wolfhagen',
    'Kassel' : 'Kassel',
    'Waldeck-Frankenberg' : 'Bad Wildungen, Frankenberg, Korbach',
    'Marburg-Biedenkopf' : 'Biedenkopf, Marburg',
    'Lahn-Dill-Kreis' : 'Dillenburg, Wetzlar',
    'Gießen' : 'Gießen, Grünberg',
    'Limburg-Weilburg': 'Limburg, Weilburg',
    'Vogelsbergkreis' : 'Alsfeld, Lauterbach',
    'Fulda' : 'Fulda, Hünfeld, Neuhof',
    'Main-Kinzig-Kreis' : 'Bad Orb, Erlensee, Gelnhausen, Steinau, Wächtersbach',
    'Hochtaunuskreis' : 'Bad Homburg',
    'Frankfurt am Main' : 'Frankfurt',
    'Wetteraukreis' : 'Friedberg/Hessen',
    'Main-Taunus-Kreis' : 'Hofheim',
    'Rheingau-Taunus-Kreis' : 'Idstein, Heidenrod, Geisenheim',
    'Offenbach Landkreis' : 'Neu-Isenburg, Seligenstadt',
    'Offenbach am Main' : 'Offenbach',
    'Wiesbaden' : 'Wiesbaden',
    'Bergstraße' : 'Bensheim, Heppenheim, Lampertheim, Viernheim',
    'Darmstadt' : 'Darmstadt',  
    'Odenwaldkreis' : 'Michelstadt',
    'Darmstadt-Dieburg' : 'Groß-Umstadt, Ober-Ramstadt, Pfungstadt',
    'Groß-Gerau' : 'Groß-Gerau, Rüsselsheim',
    'Bad Kreuznach' : 'Bad Kreuznach',
    'Mainz-Bingen' : 'Bingen',
    'Frankenthal (Pfalz)' : 'Frankenthal',
    'Birkenfeld' : 'Idar-Oberstein',
    'Ludwigshafen am Rhein' : 'Ludwigshafen',
    'Mainz' : 'Mainz',
    'Rhein-Hunsrück-Kreis' : 'Simmern',
    'Alzey-Worms' : 'Alzey, Wörrstadt',
    'Worms' : 'Worms',
#BY
    'Landsberg am Lech' : 'Landsberg',
    'Starnberg' : 'Starnberg',
    'Weilheim-Schongau' : 'Schongau, Weilheim',
    'Bad Tölz-Wolfsratshausen' : 'Geretsried',
    'Garmisch-Partenkirchen' : 'Garmisch-Partenkirchen',
    'Miesbach' : 'Miesbach',
    'Rosenheim Landkreis' : 'Bad Aibling',
    'Rosenheim' : 'Rosenheim',
    'Günzburg': 'Günzburg, Krumbach',
    'Neu-Ulm' : 'Neu-Ulm',
    # 'Unterallgäu' : 'ohne OV',
    'Memmingen': 'Memmingen',
    'Kaufbeuren' : 'Kaufbeuren',
    'Ostallgäu' : 'Füssen',
    'Kempten (Allgäu)' : 'Kempten',
    'Oberallgäu' : 'Sonthofen',
    'Lindau (Bodensee)' : 'Lindenberg, Lindau',
    'Augsburg Landkreis' : 'Schwabmünchen',
    'Augsburg' : 'Augsburg',
    'Aichbach-Friedberg' : 'Friedberg',
    'Dachau' : 'Dachau',
    'Freising' : 'Freising',
    # 'Erding' : 'ohne OV',
    'Fürstenfeldbruck' : 'Fürstenfeldbruck',
    'München' : 'München-Mitte, München-Ost, München-West',
    'München Landkreis' : 'München-Land',
    'Ebersberg' : 'Markt Schwaben',
    'Landshut Landkreis': 'Egolding',
    'Landshut' : 'Landshut',
    'Rottal-Inn' : 'Eggenfelden, Simbach',
    'Mühldorf a. Inn' : 'Mühldorf',
    'Altöttingen' : 'Altöttingen',
    'Traunstein' : 'Traunreut, Traunstein',
    'Berchtesgadener Land' : 'Berchtesgadener Land',
    'Regensburg Landkreis' : 'Laaber, Wörth',
    'Regensburg' : 'Regensburg',
    'Kelheim' : 'Kelheim',
    'Straubing-Bogen' : 'Bogen, Mallersdorf',
    'Straubing' : 'Straubing',
    'Regen' : 'Regen',
    'Deggendorf' : 'Deggendorf',
    # 'Freyung-Grafenau' : 'ohne OV',
    # 'Dingolfing-Landau' : 'ohne OV',
    'Passau Landkreis' : 'Vilshofen',
    'Passau' : 'Passau',
    'Roth' : 'Roth, Hilpolstein',
    'Weißenburg-Gunzenhausen' : 'Gunzenhausen, Treuchtlingen',
    'Eichstätt' : 'Eichstätt',
    'Donau-Ries' : 'Nördlingen, Donauwörth',
    'Neuburg-Schrobenhausen' : 'Neuburg',
    'Ingolstadt' : 'Ingolstadt',
    'Pfaffenhofen a.d. Ilm' : 'Paffenhofen',
    'Dillingen a.d. Donau' : 'Dillingen',
    'Nürnberger Land' : 'Lauf',
    'Amberg-Sulzbach' : 'Sulzbach-Rosenberg',
    'Amberg' : 'Amberg',
    'Schwandorf' : 'Nabburg, Oberviechtach, Neunburg v.W., Schwandorf',
    'Cham' : 'Cham, Roding',
    'Neumarkt i.d. OPf.' : 'Neumarkt, Parsberg',
    'Neustadt a.d. Aisch-Bad Windsheim' : 'Neustadt a.d.A.',
    'Erlangen-Höchstadt' : 'Baiersdorf',
    'Erlangen' : 'Erlangen',
    'Fürth' : 'Fürth',
    # 'Landkreis Fürth' : 'ohne OV',
    'Ansbach' : 'Ansbach',
    'Ansbach Landkreis' : 'Rothenburg, Feuchtwangen, Dinkelsbühl',
    'Nürnberg' : 'Nürnberg',
    'Schwabach' : 'Schwabach',
    'Kronach' : 'Kronach',
    'Hof' : 'Hof',
    'Hof Landkreis' : 'Naila',
    'Kulmbach' : 'Kulmbach',
    'Wunsiedel i. Fichtelgebirge': 'Marktredwitz, Selb',
    # 'Tirschenreuth' : 'ohne OV',
    'Bayreuth': 'Bayreuth',
    'Bayreuth Landkreis': 'Pegnitz',
    # 'Neustadt a.d. Waldnaab': 'ohne OV',
    'Weiden o.d. OPf.' : 'Weiden',
    'Bad Kissingen' : 'Bad Kissingen',
    'Rhön-Grabfeld' : 'Mellrichstadt',
    'Schweinfurt' : 'Schweinfurt',
    'Schweinfurt Landkreis' : 'Gerolzhofen',
    'Haßberge' : 'Haßfurt',
    'Coburg' : 'Coburg',
    # 'Landkreis Coburg' : 'ohne OV',
    'Lichtenfels' : 'Bad Staffelstein',
    'Bamberg' : 'Bamberg',
    # 'Landkreis Bamberg' : 'ohne OV',
    'Forchheim' : 'Kirchehrenbach, Forchheim',
    'Aschaffenburg': 'Alzenau, Aschaffenburg',
    'Miltenberg': 'Miltenberg, Obernburg',
    'Main-Spessart': 'Karlstadt, Lohr, Marktheidenfeld',
    'Würzburg': 'Würzburg',
    'Würzburg Landkreis' : 'Ochsenfurt',
    'Kitzingen': 'Kitzingen',
#NI
    'Braunschweig' : 'Braunschweig',
    'Salzgitter' : 'Salzgitter',
    'Wolfsburg' : 'Wolfsburg',
    'Gifhorn' : 'Gifhorn',
    'Goslar' : 'Goslar, Clausthal-Zellerfeld',
    'Helmstedt' : 'Helmstedt, Schöningen',
    'Northeim' : 'Einbeck, Northeim',
    'Peine' : 'Peine',
    'Wolfenbüttel' : 'Wolfenbüttel',
    'Göttingen' : 'Bad Lauterberg, Gieboldehausen, Göttingen, Hann.-Münden, Osterode - NI',
    'Region Hannover' : 'Burgdorf, Hannover/Langenhagen, Lehrte, Ronnenberg, Springe, Wunstorf',
    'Diepholz' : 'Bassum, Sulingen, Syke',
    'Hameln-Pyrmont' : 'Hameln',
    'Hildesheim' : 'Elze, Hildesheim, Sarstedt',
    'Holzminden' : 'Holzminden',
    'Nienburg (Weser)' : 'Hoya, Nienburg',
    'Schaumburg' : 'Bückeburg, Rinteln, Stadthagen',
    'Celle' : 'Celle',
    'Cuxhaven' : 'Cuxhaven',
    'Harburg' : 'Stelle-Winsen',
    'Lüchow-Dannenberg' : 'Lüchow-Dannenberg',
    'Lüneburg' : 'Lüneburg',
    'Osterholz' : 'Osterh.-Scharmbeck',
    'Rotenburg (Wümme)' : 'Rotenburg',
    'Heidekreis' : 'Fallingbostel-Walsrode, Soltau',
    'Stade' : 'Buxtehude, Kutenholz, Stade',
    'Uelzen' : 'Uelzen',
    'Verden' : 'Achim, Verden',
    'Delmenhorst' : 'Delmenhorst',
    'Emden' : 'Emden',
    'Oldenburg (Oldb)' : 'Oldenburg - NI',
    'Osnabrück Landkreis' : 'Bad Essen, Melle, Quakenbrück',
    'Wilhelmshaven' : 'Wilhelmshaven',
    'Ammerland' : 'Westerstede',
    'Aurich' : 'Aurich, Norden',
    'Cloppenburg' : 'Coppenburg',
    'Emsland' : 'Lingen, Meppen, Papenburg-Aschend',
    'Friesland' : 'Jever, Varel',
    'Grafschaft Bentheim' : 'Nordhorn',
    'Leer' : 'Leer',
    'Oldenburg' : 'Hude-Bookholzberg, Wardenburg',
    'Osnabrück' : 'Osnabrück',
    'Vechta' : 'Lohne',
    'Wesermarsch' : 'Nordenham',
    'Wittmund' : '',
    'Bremen' : 'Bremen Mitte, Bremen Nord, Bremen Ost, Bremen Süd',
    'Bremerhaven' : 'Bremerhaven',
#SH
    'Flensburg' : 'Flensburg',
    'Kiel' : 'Kiel',
    'Lübeck' : 'Lübeck',
    'Neumünster' : 'Neumünster',
    'Dithmarschen' : 'Heide, Meldorf, Burg-Hochdonn',
    'Herzogtum Lauenburg' : 'Lauenburg, Mölln, Ratzeburg',
    'Nordfriesland' : 'Niebüll, Husum, Tönning',
    'Ostholstein' : 'Eutin, Neustadt, Oldenburg',
    'Pinneberg' : 'Barmstedt, Elmshorn, Pinneberg',
    'Plön' : 'Plön, Preetz',
    'Rendsburg-Eckernförde' : 'Eckernförde, Louiselund, Rendsburg',
    'Schleswig-Flensburg' : 'Schleswig, Sörup',
    'Segeberg' : 'Bad Segeberg, Kaltenkrichen, Wahlstedt',
    'Steinburg' : 'Itzehoe',
    'Stormarn' : 'Ahrensburg, Bad Oldesloe',
    'Hamburg' : 'Hamburg Mitte, Hamburg Nord, Hamburg-Altona, Hamburg-Bergedorf, Hamburg-Eimsbüttel, Hamburg-Harburg, Hamburg-Wandsbek',
    'Rostock' : 'Rostock',
    'Schwerin' : 'Schwerin',
    'Mecklenburgische Seenplatte' : 'Demmin, Neubrandenburg, Neustrelitz, Waren',
    'Rostock Landkreis' : 'Bad Doberan, Güstrow',
    'Vorpommern-Rügen' : 'Barth, Bergen auf Rügen, Stralsund',
    'Nordwestmecklenburg' : 'Gadebusch, Wismar',
    'Vorpommern-Greifswald' : 'Greifswald, Pasewalk, Wolgast',
    'Ludwigslust-Parchim' : 'Ludwigslust, Parchim', 
#TH
    'Chemnitz' : 'Chemnitz',
    'Vogtlandkreis' : 'Reichenbach, Plauen',
    'Zwickau' : 'Zwickau',
    'Dresden' : 'Dresden',
    'Erzgebirgskreis' : 'Annaberg, Aue-Schwarzenberg',
    'Bautzen' : 'Bautzen, Kamenz',
    'Görlitz' : 'Görlitz, Zittau',
    'Mittelsachsen' : 'Döbeln, Freiberg',
    'Meißen' : 'Radebeul, Riesa',
    'Sächsische Schweiz-Osterzgebirge' : 'Dippoldiswalde, Pirna',
    'Leipzig' : 'Leipzig',
    'Leipzig Landkreis' : 'Borna, Grimma', #Landkreis 363
    'Nordsachsen' : 'Eilenburg, Torgau',
    'Erfurt' : 'Erfurt',
    'Gera' : 'Gera',
    'Jena' : 'Apolda',
    'Suhl' : 'Suhl',
    'Weimar' : 'Apolda',
    'Eisenach' : 'Eisenach',
    'Eichsfeld' : 'Heiligenstadt',
    'Nordhausen' : 'Nordhausen',
    'Wartburgkreis' : 'Eisenach',
    'Unstrut-Hainich-Kreis' : 'Heiligenstadt',
    'Kyffhäuserkreis' : 'Sondershausen',
    'Schmalkalden-Meiningen' : 'Suhl',
    'Gotha' : 'Gotha',
    'Sömmerda' : 'Sonderhausen',
    'Hildburghausen' : 'Sonneberg',
    'Ilm-Kreis' : 'Erfurt',
    'Weimarer Land' : 'Apolda',
    'Sonneberg' : 'Sonneberg',
    'Saalfeld-Rudolstadt' : 'Rudolstadt/Saalfeld',
    'Saale-Holzland-Kreis' : 'Apolda',
    'Saale-Orla-Kreis' : 'Gera',
    'Greiz' : 'Gera',
    'Altenburger Land' : 'Altenburg',
#NW
    'Städteregion Aachen' : 'Aachen, Alsdorf, Eschweiler, Herzogenrath, Simmerath, Stolberg',
    'Bielefeld' : 'Bielefeld',
    'Bochum' : 'Bochum',
    'Bonn' : 'Bonn, Beuel',
    'Borken' : 'Ahaus, Bocholt/Borken, Gronau',
    'Bottrop' : 'Bottrop',
    'Coesfeld' : 'Coesfeld, Dülmen, Havixbeck, Lüdinghausen',
    'Dortmund' : 'Dortmund',
    'Duisburg' : 'Dinslaken, Duisburg',
    'Düren' : 'Düren, Hürtgenwald, Jülich, Nörvenich',
    'Düsseldorf' : 'Düsseldorf',
    'Ennepe-Ruhr-Kreis' : 'Hattingen, Schwelm, Wetter, Witten',
    'Essen' : 'Essen',
    'Euskirchen' : 'Euskirchen, Schleiden',
    'Gelsenkirchen' : 'Gelsenkirchen',
    'Gütersloh' : 'Gütersloh, Halle',
    'Hagen' : 'Hagen',
    'Hamm' : 'Hamm',
    'Heinsberg' : 'Erkelenz, Hückelhoven, Übach-Palenberg',
    'Herford' : 'Bünde, Herford, Vlotho',
    'Herne' : 'Herne, Wanne-Eickel',
    'Hochsauerlandkreis' : 'Arnsberg, Brilon, Hallenberg-Hesborn, Meschede',
    'Höxter' : 'Höxter, Warburg',
    'Kleve' : 'Emmerich, Geldern, Kleve',
    'Köln' :  'Köln Nord-West, Köln-Ost, Köln Porz',
    'Krefeld' : 'Krefeld',
    'Leverkusen' : 'Leverkusen',
    'Lippe' : 'Detmold, Lemgo',
    'Märkischer Kreis' : 'Altena, Balve, Halver, Iserlohn, Lüdenscheid',
    'Mettmann' : 'Haan, Heiligenhaus/Wülfrath, Hilden, Ratingen, Velbert',
    'Minden-Lübbecke' : 'Lübbecke, Minden',
    'Mönchengladbach' : 'Mönchengladbach',
    'Mülheim an der Ruhr' : 'Mülheim',
    'Münster' : 'Münster',
    'Oberbergischer Kreis' : 'Bergneustadt, Gummersbach, Hückeswagen, Waldbröl',
    'Oberhausen' : 'Oberhausen',
    'Olpe' : 'Attendorn, Lennestadt, Olpe',
    'Paderborn' : 'Büren, Paderborn',
    'Recklinghausen' : 'Castrop-Rauxel, Datteln, Gladbeck/Dorsten, Haltern am See, Herten, Marl, Recklinghausen, Waltrop',
    'Remscheid' : 'Remscheid',
    'Rhein-Erft-Kreis' : 'Bergheim, Brühl',
    'Rheinisch-Bergischer Kreis' : 'Bergisch Gladbach, Wermelskirchen',
    'Rhein-Kreis Neuss' : 'Grevenbroich, Neuss',
    'Rhein-Sieg-Kreis' : 'Bad Honnef, Bornheim, Siegburg',
    'Siegen-Wittgenstein' : 'Bad Berleburg, Siegen',
    'Soest' : 'Lippstadt, Soest',
    'Solingen' : 'Solingen',
    'Steinfurt' : 'Greven, Ibbenbüren, Lengerich, Rheine',
    'Unna' : 'Kamen-Bergkamen, Lünen, Unna-Schwerte, Werne',
    'Viersen' : 'Kempen, Nettetal, Viersen',
    'Warendorf' : 'Beckum, Oelde, Warendorf',
    'Wesel' : 'Moers, Wesel',
    'Wuppertal' : 'Wuppertal',
#BE
    'Berlin Reinickendorf' : 'Berlin Reinickendorf',
    'Berlin Charlottenburg-Wilmersdorf' : 'Berlin Charlottenburg-Wilmersdorf',
    'Berlin Treptow-Köpenick' : 'Berlin Treptow-Köpenick',
    'Berlin Pankow' : 'Berlin Pankow',
    'Berlin Neukölln' : 'Berlin Neukölln',
    'Berlin Lichtenberg' : 'Berlin Lichtenberg',
    'Berlin Marzahn-Hellersdorf' : 'Berlin Marzahn-Hellersdorf',
    'Berlin Spandau' : 'Berlin Spandau',
    'Berlin Steglitz-Zehlendorf' : 'Berlin Steglitz-Zehlendorf',
    'Berlin Mitte' : 'Berlin-Mitte',
    'Berlin Friedrichshain-Kreuzberg' : 'Berlin Friedrichshain-Kreuzberg',
    'Berlin Tempelhof-Schöneberg' : 'Berlin Tempelhof-Schöneberg',
    'Dessau-Roßlau' : 'Dessau',
    'Halle (Saale)' : 'Halle (Saale)',
    'Wittenberg' : 'Wittenberg',
    'Altmarkkreis Salzwedel' : 'Salzwedel',
    'Börde' : 'Oschersleben',
    'Burgenlandkreis' : 'Naumburg',
    'Jerichower Land' : 'Burg',
    'Magdeburg' : 'Magdeburg',
    'Stendal' : 'Stendal',
    'Barnim' : 'Eberswalde',
    'Frankfurt (Oder)' : 'Frankfurt (Oder)',
    'Cottbus' : 'Cottbus',
    'Brandenburg an der Havel' : 'Brandenburg an der Havel',
    'Dahme-Spreewald' : 'Lübben (Spreewald)',
    'Elbe-Elster' : 'Herzberg',
    'Havelland' : 'Rathenow',
    'Märkisch-Oderland' : 'Seelow',
    'Oberhavel' : 'Gransee',
    'Oberspreewald-Lausitz' : 'Senftenberg',
    'Oder-Spree' : 'Fürstenwalde/Spree',
    'Ostprignitz-Ruppin' : 'Neuruppin',
    'Potsdam-Mittelmark' : 'Bad Belzig, Potsdam',
    'Prignitz' : 'Wittenberge',
    'Spree-Neiße' : 'Forst (Lausitz)',
    'Teltow-Fläming' : 'Luckenwalde',
    'Uckermark' : 'Prenzlau',
    'Anhalt-Bitterfeld' : 'Wolfen-Bitterfeld',
    'Burgenlandkreis' : 'Naumburg, Weißenfels',
    'Mansfeld-Südharz' : 'Sangerhausen',
    'Saalekreis' : 'Merseburg',
    'Salzlandkreis' : 'Bernburg, Calbe, Staßfurt',
    'Harz' : 'Halberstadt, Quedlinburg',
}

def isToday(obj):
    return obj['time'] == date.today()

def isYesterday(obj):
    return obj['time'] == (date.today() - timedelta(days=1))

def query_DB():
    query_result = Data.objects.filter(time__gte=(date.today() - timedelta(days=1))).order_by('gen').values()
    result_today = list(filter(isToday, query_result))
    result_yesterday = list(filter(isYesterday, query_result))
    return result_today, result_yesterday

def query_DB_BL(lv):
    query_result_today_BL = Data_BL.objects.filter(time__exact=date.today(),objectid__in=lv).order_by('gen').values()
    return query_result_today_BL

def query_DB_BL_YT(objid):
    query_result = Data_BL.objects.filter(time__gte=(date.today() - timedelta(days=1))).order_by('gen').values()
    result_today = list(filter(isToday, query_result))
    result_yesterday = list(filter(isYesterday, query_result))
    return result_today, result_yesterday

def query_DB_Chart(rb_name, time_len=28):
    return Data.objects.filter(objectid__in=rb_ids[rb_name]).filter(time__gte=(date.today() - timedelta(days=time_len))).order_by('time').values()

def query_DB_Chart_BL(time_len=14):
    return Data_BL.objects.filter(time__gte=(date.today() - timedelta(days=time_len))).order_by('time').values()

def query_DB_Chart_statistic(rb_name, begin, end):
    return Data_BL.objects.filter(objectid=rb_name).filter(time__gte=begin).filter(time__lte=end).order_by('time').values() if rb_name.isnumeric() else Data.objects.filter(objectid__in=rb_ids[rb_name]).filter(time__gte=begin).filter(time__lte=end).order_by('time').values()

# def get_rb_from_id(n):
#     tmp=''
#     for item in rb_ids:
#         if n in rb_ids[item]:
#             # print(n,item)
#             tmp = item
#     # print(list(filter(lambda x : n in x,rb_ids)))
#     return tmp

def add_ovnames(n, y):
    n['ovs'] = ov_dict[n['gen']] if n['gen'] in ov_dict else ''
    n['change'] = get_change(n,y)
    n['color'] = table_color(n)
    # n['rb'] = get_rb_from_id(n['objectid'])
    # print(n['rb'])
    return n

def get_change(new, old):
    up = '<svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-caret-up-square-fill" fill="#FF0000" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm4 9a.5.5 0 0 1-.374-.832l4-4.5a.5.5 0 0 1 .748 0l4 4.5A.5.5 0 0 1 12 11H4z"/></svg>'
    same = '<svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-caret-right-square-fill" fill="#FFC300" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm5.5 10a.5.5 0 0 0 .832.374l4.5-4a.5.5 0 0 0 0-.748l-4.5-4A.5.5 0 0 0 5.5 4v8z"/></svg>'
    down = '<svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-caret-down-square-fill" fill="#94D302" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm4 4a.5.5 0 0 0-.374.832l4 4.5a.5.5 0 0 0 .748 0l4-4.5A.5.5 0 0 0 12 6H4z"/></svg>'

    # try:
    #     diff = ((new['cases']-old['cases'])/old['cases'])*100
    #     print(new['gen'],diff,new['cases'],old['cases'])
    #     return up if diff > 5  else down if diff < -5 else same
    # except:
    # return up if round(new['cases'],1) > round(old['cases'],1) else down if round(new['cases'],1) < round(old['cases'],1) else same
    return up if round(new['cases'],1) > round(old['cases'],1) else down if round(new['cases'],1) < round(old['cases'],1) else same

def table_color(n):
    return '#FF0000' if n['phase']==1 else '#FFC300' if n['phase']==2 else '#94D302'
    # return '#FF0000' if n['phase']==1 else '#FFC300' if n['phase']==2 else '#FFFF00' if n['phase']==3 else '#94D302'

def add_ov(rb_name, query_result_today, query_result_yesterday):
    rb_cases_today = list(filter(lambda objid : objid['objectid'] in rb_ids[rb_name], query_result_today))
    rb_cases_yesterday = list(filter(lambda objid : objid['objectid'] in rb_ids[rb_name], query_result_yesterday))
    return list(map(add_ovnames ,rb_cases_today, rb_cases_yesterday))

def get_neighbor(rb_name, query_result_today, query_result_yesterday):
    neighbor_cases_today = list(filter(lambda objid : objid['objectid'] in rb_nachbar_ids[rb_name], query_result_today))
    neighbor_cases_yesterday = list(filter(lambda objid : objid['objectid'] in rb_nachbar_ids[rb_name], query_result_yesterday))
    return list(map(add_ovnames ,neighbor_cases_today, neighbor_cases_yesterday))

def wrapper(rb_list, query_result_today, query_result_yesterday):
    result = []
    for item in rb_list:
        result += add_ov(item,query_result_today,query_result_yesterday)
        # for a in result:
        #     result[result.index(a)]['rb'] = item
    return [i for n, i in enumerate(result) if i not in result[n + 1:]] ##works but lets think about better?

def generate_dataset(corona_data, dataset_len=28):
    dataset = []
    cases = {}
    color = {}
    counter = 0

    def empty_list(n):
        return [''] * n

    def get_cases(n):
        return list(map(lambda x : round(x['cases'],1),n))

    label = list(set(map(lambda x : x['time'].strftime("%d.%m.%Y"),corona_data)))
    label.sort(key = lambda date: datetime.strptime(date, '%d.%m.%Y'))
    label_len = len(label) if len(label) < dataset_len else dataset_len
    kreise = list(set(map(lambda x : x['gen'],corona_data)))
    kreise.sort()

    for kreis in kreise:
        color[kreis] = color_list[counter]
        counter += 1
        cases[kreis] = get_cases(list(filter(lambda x : x['gen']==kreis,corona_data)))

    for item in cases:
        dataset.append({'data': empty_list(label_len-len(cases[item])) + cases[item][-label_len:],'borderColor': color[item],'label':item})
    return label[-label_len:], dataset

def find_key(input_dict, value):
    for key in input_dict:
        if value in input_dict[key]:
            return key

def get_all_kreise():
    all_kres = Data.objects.filter(time__exact=date.today()).order_by('gen')
    list_of_kreis = {}
    def url_converter(rb_name):
        converted_rb = rb_name
        if rb_name == 'Frankfurt (Oder)':
            converted_rb = 'frankfurt(oder)'
        elif rb_name == 'Bad Kreuznach':
            converted_rb = 'badkreuznach'
        elif rb_name == 'Neustadt a.d.W.':
            converted_rb = 'neustadt'
        elif rb_name == 'Bad Tölz':
            converted_rb = 'badtölz'
        return converted_rb

    for item in all_kres:
        rb = find_key(rb_ids, int(item.get_objectid()))
        lv = find_key(lv_liste, rb)
        list_of_kreis[item.get_name()] = {"LV": lv, "cRB" : url_converter(rb), "RB" : rb}
    return list_of_kreis

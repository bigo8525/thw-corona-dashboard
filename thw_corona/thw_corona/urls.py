"""thw_corona URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hinweise/',
         views.hinweis,
         name='hinweis'
         ),
    path('statistiken/',
         views.statistik,
         name='statistik'
         ),
     path('uebersicht/',
         views.uebersicht,
         name='uebersicht'
         ),
    path('de/',
         views.de_list,
         name='de'
         ),
    path('',
         views.landing_page,
         name='index'
         ),
    path('<str:lv_name>/',
         views.lv_list,
         name='lv'
         ),
    path('<str:lv_name>/<str:rb_name>',
         views.landing_page,
         name='rb'
         ),
]
# Corona Dashboard

## Goal of this project

This project was created to map the corona cases in every city and county to the pandemic plan of the Federal Agency for Technical Relief (THW) in Germany.
The dashboard shows an easy overview for everyone in which phase of the plan each local chapter is.

## How does this works

The corona cases are beeing scraped every morning by the helper script getData.py. This script get the data and saves it into a mysql database.
The collector script save the exact cases and updates its value for the actual day if possible. This script can be run by cronjob.
For the corona data published by the used API it is the best to perform every day in the timeslot from 4 to 11 am. In this timerange most of the time the new
values should be available.
An django application is used to visulize the data fetch by the collector script.
For maximum performance a default memcached is used with the django application.

## Requirements

For the django application the following pip packages are used in an virtual environment:

- django
- mysqlclient
- pyyaml
- gunicorn
- requests
- mysql-connector
- pylibmc
- python-dateutil

```
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements.txt
```

This project could be hostet with nginx, gunicorn and mysql.
Before the start some passwords and user must be set in getData.py and settings.py.

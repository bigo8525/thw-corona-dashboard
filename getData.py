import requests
import json
import mysql.connector
from datetime import date

mysql_pass = ""
query_url = 'https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_Landkreisdaten/FeatureServer/0/query?where=1%3D1&outFields=OBJECTID,GEN,last_update,cases7_per_100k&returnGeometry=false&outSR=4326&f=json'
query_url_bl = 'https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/Coronaf%C3%A4lle_in_den_Bundesl%C3%A4ndern/FeatureServer/0/query?where=1%3D1&outFields=LAN_ew_GEN,OBJECTID,cases7_bl_per_100k&returnGeometry=false&outSR=4326&f=json'
cnx = mysql.connector.connect(user='corona', password=mysql_pass,
                             host='127.0.0.1',
                             database='corona_data')
all_data = []

def getData(url):
    global all_data
    all_data = requests.get(url).json()['features']
    return all_data

def get_array(index):
    return index['attributes']

def jsonToDict(json_obj):
    return list(map(get_array, json_obj))

def calculate_phase(cases):
    return 3 if cases < 50 else 2 if (cases > 50 and cases < 350) else 1

def switching(value):
    switcher = {
        58:'Osnabrück',
        128:'Offenbach',
        139:'Kassel',
        173:'Kaiserslautern',
        186:'Heilbronn',
        194:'Karlsruhe',
        239:'München',
        242:'Rosenheim',
        252:'Landshut',
        253:'Passau',
        265:'Regensburg',
        272:'Bamberg',
        273:'Bayreuth',
        274:'Coburg',
        276:'Hof',
        286:'Ansbach',
        288:'Fürth',
        296:'Aschaffenburg',
        303:'Schweinfurt',
        304:'Würzburg',
        310:'Augsburg',
        363:'Leipzig',
        347:'Rostock',
    }
    return switcher.get(value,(-1))
        
def proof_vaule(value):
    cases = ''
    value['GEN'] = (switching(value['OBJECTID']) + ' Landkreis') if switching(value['OBJECTID']) != (-1) else value['GEN']
    try:
        cursor = cnx.cursor()
        cursor.execute("SELECT cases FROM thw_corona_data WHERE objectid = %s and time = %s",
                       (value['OBJECTID'], date.today(),))
        cases = cursor.fetchone()[0]
    except:
        insertData(value)
    if cases != value['cases7_per_100k']:
        updateData(value)

def updateData(value):
    cnx.cursor().execute("UPDATE thw_corona_data SET cases = %s, phase = %s WHERE objectid = %s and time = %s", (str(
        float(value['cases7_per_100k'])), calculate_phase(value['cases7_per_100k']), value['OBJECTID'], date.today()))

def insertData(value):
    cnx.cursor().execute(("INSERT INTO thw_corona_data(objectid,gen,cases,phase,time) VALUES(%s,%s,%s,%s,%s)"), (value['OBJECTID'], str(
        value['GEN']), str(float(value['cases7_per_100k'])), calculate_phase(value['cases7_per_100k']), date.today()))

def saveToDB(values, status):
    list(map(proof_vaule, values)) if status == 0 else list(map(proof_vaule_bl, values))
    cnx.commit()
    cnx.close()

def proof_vaule_bl(value):
    cases = ''
    try:
        cursor = cnx.cursor()
        cursor.execute("SELECT cases FROM thw_corona_data_bl WHERE objectid = %s and time = %s",
                       (value['OBJECTID'], date.today(),))
        cases = cursor.fetchone()[0]
    except:
        insertData_bl(value)
    if cases != value['cases7_bl_per_100k']:
        updateData_bl(value)

def updateData_bl(value):
    cnx.cursor().execute("UPDATE thw_corona_data_bl SET cases = %s WHERE objectid = %s and time = %s", (str(
        float(value['cases7_bl_per_100k'])), value['OBJECTID'], date.today()))

def insertData_bl(value):
    cnx.cursor().execute(("INSERT INTO thw_corona_data_bl(objectid,gen,cases,time) VALUES(%s,%s,%s,%s)"), (value['OBJECTID'], str(
        value['LAN_ew_GEN']), str(float(value['cases7_bl_per_100k'])), date.today()))

def print_result(values):
    for i in range(len(values)):
        print(values[i]['OBJECTID'], values[i]['GEN'], values[i]
              ['cases7_per_100k'], calculate_phase(values[i]['cases7_per_100k']))

def print_result_bl(values):
    for i in range(len(values)):
        print(values[i]['OBJECTID'], values[i]['LAN_ew_GEN'], values[i]
              ['cases7_bl_per_100k'])

if __name__ == '__main__':
    # print_result(jsonToDict(getData(query_url)))
    saveToDB(jsonToDict(getData(query_url)), 0)
    cnx.connect()
    saveToDB(jsonToDict(getData(query_url_bl)), 1)
    # print_result_bl(jsonToDict(getData(query_url_bl)))